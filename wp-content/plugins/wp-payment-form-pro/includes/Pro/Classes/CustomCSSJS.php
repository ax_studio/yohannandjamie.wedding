<?php

namespace WPPayForm\Pro\Classes;


use WPPayForm\Classes\AccessControl;

if (!defined('ABSPATH')) {
    exit;
}

/**
 * Form Info Handler Class
 * @since 1.1.1
 */
class CustomCSSJS
{
    public function registerEndpoints()
    {
        add_action('wp_ajax_wpf_css_js_endpoints', array($this, 'routeAjaxMaps'));
        add_action('wppayform/wppayform_adding_assets', array($this, 'printJS'));
        add_action('wppayform/form_render_before', array($this, 'printCSS'));
    }

    public function routeAjaxMaps()
    {
        $routes = array(
            'get_settings' => 'getSettings',
            'save_settings' => 'saveSettings'
        );
        $route = sanitize_text_field($_REQUEST['route']);

        if (isset($routes[$route])) {
            AccessControl::checkAndPresponseError($route, 'form_settings');
            do_action('wppayform/doing_ajax_submissions_' . $route);
            $this->{$routes[$route]}();
            return;
        }
    }

    public function getSettings()
    {
        $formId = absint($_REQUEST['form_id']);
        $customCss = get_post_meta($formId, '_wpf_custom_css', true);
        $customJs = get_post_meta($formId, '_wpf_custom_js', true);

        wp_send_json_success(array(
            'custom_css' => $customCss,
            'custom_js' => $customJs
        ), 200);
    }

    public function saveSettings()
    {
        $formId = absint($_REQUEST['form_id']);

        $css = $_REQUEST['custom_css'];
        $js = $_REQUEST['custom_js'];
        $css = wp_strip_all_tags($css);
        $js = wp_unslash($js);
        update_post_meta($formId, '_wpf_custom_css', $css);
        update_post_meta($formId, '_wpf_custom_js', $js);

        wp_send_json_success(array(
            'message' => __('Custom CSS and JS successfully saved', 'wppayform')
        ), 200);
    }

    public function printJS($form)
    {
        $customJS = get_post_meta($form->ID, '_wpf_custom_js', true);
        if($customJS) {
            add_action('wp_footer', function () use ($form, $customJS) {
                ?>
                <script type="text/javascript">
                    jQuery(document.body).on('wp_payform_inited_<?php echo $form->ID; ?>', function (event, data, formConfig) {
                        var $form = jQuery(data[0]);
                        var $ = jQuery;
                        try {
                            <?php echo $customJS; ?>
                        } catch (e) {
                            console.warn('Error in custom JS of WPPayfFrom ID: '+$form.data('wpf_form_id'));
                            console.error(e);
                        }
                    });
                </script>
                <?php
            }, 100, 1);
        }
    }

    public function printCSS($form)
    {
        $customCSS = get_post_meta($form->ID, '_wpf_custom_css', true);
        if($customCSS) {
            ?>
                <style type="text/css">
                    <?php echo $customCSS; ?>
                </style>
            <?php
        }
    }

}