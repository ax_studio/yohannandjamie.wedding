<?php
if(!class_exists('WPPayformProUpdateChecker')) {
	require 'WPPayformProUpdateChecker.php';
}

if(!class_exists('WPPayFormProUpdater')) {
	require 'WPPayFormProUpdater.php';
}

// Kick off our EDD class
new WPPayformProUpdateChecker( array(
	// The plugin file, if this array is defined in the plugin
	'plugin_file' => WPPAYFORM_MAIN_FILE,
	// The current version of the plugin.
	// Also need to change in readme.txt and plugin header.
	'version' => WPPAYFORM_VERSION,
	// The main URL of your store for license verification
	'store_url' => 'https://wpmanageninja.com',
	// Your name
	'author' => 'WP Manage Ninja',
	// The URL to renew or purchase a license
	'purchase_url' => 'https://wpmanageninja.com/downloads/wppayform-pro-wordpress-payments-form-builder/',
	// The URL of your contact page
	'contact_url' => 'https://wpmanageninja.com/contact',
	// This should match the download name exactly
	'item_id' => '39549',
	// The option names to store the license key and activation status
	'license_key' => '_wppayform_pro_license_key',
	'license_status' => '_wppayform_pro_license_status',
	// Option group param for the settings api
	'option_group' => '_wppayform_pro_license',
	// The plugin settings admin page slug
	'admin_page_slug' => 'wppayform.php',
	// If using add_menu_page, this is the parent slug to add a submenu item underneath.
	'activate_url' => admin_url('admin.php?page=wppayform.php#/settings/licensing'),
	// The translatable title of the plugin
	'plugin_title' => __( 'WPPay Form pro', 'wppayform' ),
	'menu_slug' => 'wppayform',
	'menu_title' => __('WPPay Form pro', 'wppayform'),
    // How much time (in seconds) the updater won't check the license.
    'cache_time' => 48 * 60 * 60
));